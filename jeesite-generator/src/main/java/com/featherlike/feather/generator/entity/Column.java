package com.featherlike.feather.generator.entity;

public class Column {

	private String name;
	private String type;
	private String length;
	private String precision;
	private String notnull;
	private String pk;
	private String comment;
	private String listPage;
	private String queryPage;
	private String modifyPage;
	private String asLink;
	private String dictType;

	public Column(String name, String type, String length, String precision,
			String notnull, String pk, String comment, String listPage,
			String queryPage, String modifyPage, String asLink, String dictType) {
		this.name = name;
		this.type = type;
		this.length = length;
		this.precision = precision;
		this.notnull = notnull;
		this.pk = pk;
		this.comment = comment;
		this.listPage = listPage;
		this.queryPage = queryPage;
		this.modifyPage = modifyPage;
		this.asLink = asLink;
		this.dictType = dictType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public String getNotnull() {
		return notnull;
	}

	public void setNotnull(String notnull) {
		this.notnull = notnull;
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getListPage() {
		return listPage;
	}

	public void setListPage(String listPage) {
		this.listPage = listPage;
	}

	public String getQueryPage() {
		return queryPage;
	}

	public void setQueryPage(String queryPage) {
		this.queryPage = queryPage;
	}

	public String getModifyPage() {
		return modifyPage;
	}

	public void setModifyPage(String modifyPage) {
		this.modifyPage = modifyPage;
	}

	public String getAsLink() {
		return asLink;
	}

	public void setAsLink(String asLink) {
		this.asLink = asLink;
	}

	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}
}
