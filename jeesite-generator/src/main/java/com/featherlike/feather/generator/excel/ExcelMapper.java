/*****************************************************************
 *仿佛兮若轻云之蔽月，飘飘兮若流风之回雪
 *@filename DBExcelMapper.java
 *@author WYY
 *@date 2013年11月24日
 *@copyright (c) 2013  wyyft@163.com All rights reserved.
 *****************************************************************/
package com.featherlike.feather.generator.excel;

import java.util.List;
import java.util.Map;

import com.featherlike.feather.generator.entity.Column;
import com.featherlike.feather.generator.entity.Table;

public interface ExcelMapper {
	Map<Table, List<Column>> createTableMap(String inputPath);
}